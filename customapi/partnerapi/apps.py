from django.apps import AppConfig


class PartnerapiConfig(AppConfig):
    name = 'partnerapi'
